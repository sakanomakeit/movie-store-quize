# Movie Store Quiz

This project is movie store with Angular ( version 12.2.16 )

## Tools

- Angular (12.2.16)
- Ngxs
- TailwindCss
- Fontawesome (free version)

## Features

- User mode
  - Add movie to cart
  - view detail movie
  - search movie
  - management cart (increase item, reduce item, remove item, clear item)
  - checkout cart
- Admin mode
  - view movie list all
  - view movie list not price specified
  - search movie
  - edit price movie

## Update (14/05/22)

- fixed bug pagination
- fixed bug page of number
- update pagination disabled. When there is no next or previous page
- add button remove price in admin mode
- move control in page to store control

## How to run project

1). Install tools

```sh
npm install
```

2). Run angular project

```sh
ng serve
```

3). Project run on "localhost:4200"

4).and Enjoin
