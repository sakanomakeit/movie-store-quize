import { AdminsModule } from './pages/admins/admins.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { CartState } from './shared/stores/cart/cart.store';
import { HttpClientModule } from '@angular/common/http';
import { MovieListState } from './shared/stores/movie-list/movie-list.store';
import { NavbarModule } from './shared/components/navbar/navbar.module';
import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { UsersModule } from './pages/users/users.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgxsModule.forRoot([MovieListState, CartState]),
    NgxsStoragePluginModule.forRoot({
      key: [MovieListState, CartState],
    }),
    // Pages
    UsersModule,
    AdminsModule,
    // Components
    NavbarModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
