import { ChangeSearchText } from './shared/stores/movie-list/movie-list.action';
import { Component } from '@angular/core';
import { Store } from '@ngxs/store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor(private readonly store: Store) {
    store.dispatch(new ChangeSearchText(''));
  }
}
