import { CartModule } from './cart/cart.module';
import { CommonModule } from '@angular/common';
import { MoviesDashboardModule } from './movies-dashboard/movies-dashboard.module';
import { NgModule } from '@angular/core';
import { UsersComponent } from './users.component';
import { UsersRoutingModule } from './users-routing.module';

@NgModule({
  imports: [
    CommonModule,
    UsersRoutingModule,
    // Pages
    MoviesDashboardModule,
    CartModule,
  ],
  declarations: [UsersComponent],
  exports: [UsersComponent],
})
export class UsersModule {}
