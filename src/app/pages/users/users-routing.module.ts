import { RouterModule, Routes } from '@angular/router';

import { CartComponent } from './cart/cart.component';
import { MoviesDashboardComponent } from './movies-dashboard/movies-dashboard.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', redirectTo: 'movies-dashboard', pathMatch: 'full' },
      { path: 'movies-dashboard', component: MoviesDashboardComponent },
      { path: 'cart', component: CartComponent },
      { path: '**', redirectTo: 'movies-dashboard', pathMatch: 'full' },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersRoutingModule {}
