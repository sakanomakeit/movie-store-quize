import { Component, OnInit } from '@angular/core';

import { ActionAnimation } from 'src/app/shared/animations/action.animation';
import { ActionPage } from 'src/app/shared/enums/action-page.enum';

@Component({
  selector: 'app-movies-dashboard',
  templateUrl: './movies-dashboard.component.html',
  styleUrls: ['./movies-dashboard.component.scss'],
  animations: [...ActionAnimation],
})
export class MoviesDashboardComponent implements OnInit {
  changePage: ActionPage = ActionPage.WAIT;

  constructor() {}

  ngOnInit() {}

  get ActionPage(): typeof ActionPage {
    return ActionPage;
  }
}
