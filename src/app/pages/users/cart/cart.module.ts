import { CartComponent } from './cart.component';
import { CartComponentsModule } from 'src/app/components/users/cart-components/cart-components.module';
import { CommonModule } from '@angular/common';
import { ModalModule } from '../../../shared/components/modal/modal.module';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [CommonModule, RouterModule, ModalModule, CartComponentsModule],
  declarations: [CartComponent],
  exports: [CartComponent],
})
export class CartModule {}
