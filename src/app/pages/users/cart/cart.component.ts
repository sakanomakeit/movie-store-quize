import {
  ClearItem,
  IncreaseItemNumber,
  ReduceItemNumber,
  RemoveItemFromCart,
} from './../../../shared/stores/cart/cart.action';
import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';

import { CartState } from './../../../shared/stores/cart/cart.store';
import { CartStateModel } from 'src/app/shared/stores/cart/cart.state';
import { IMG_URL } from './../../../shared/services/movie.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {
  @Select(CartState)
  $cart: Observable<CartStateModel>;
  isCheckout: boolean = false;

  imgUrl: string = IMG_URL;

  constructor(private store: Store) {}

  ngOnInit() {}

  onIncrease(index: number): void {
    this.store.dispatch(new IncreaseItemNumber(index));
  }

  onReduce(index: number): void {
    this.store.dispatch(new ReduceItemNumber(index));
  }

  onRemoveItem(index: number): void {
    this.store.dispatch(new RemoveItemFromCart(index));
  }

  onClearCart(): void {
    this.store.dispatch(new ClearItem());
  }
}
