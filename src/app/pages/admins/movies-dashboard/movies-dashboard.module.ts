import { CommonModule } from '@angular/common';
import { MoviesDashboardComponent } from './movies-dashboard.component';
import { MoviesDashboardComponentsModule } from 'src/app/components/admins/movies-dashboard-components/movies-dashboard-components.module';
import { NgModule } from '@angular/core';
import { PaginationModule } from './../../../shared/components/pagination/pagination.module';

@NgModule({
  imports: [CommonModule, MoviesDashboardComponentsModule, PaginationModule],
  declarations: [MoviesDashboardComponent],
  exports: [MoviesDashboardComponent],
})
export class MoviesDashboardModule {}
