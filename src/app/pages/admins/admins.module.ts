import { AdminsComponent } from './admins.component';
import { AdminsRoutingModule } from './admins-routing.module';
import { CommonModule } from '@angular/common';
import { MoviesDashboardModule } from './movies-dashboard/movies-dashboard.module';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [CommonModule, AdminsRoutingModule, MoviesDashboardModule],
  declarations: [AdminsComponent],
  exports: [AdminsComponent],
})
export class AdminsModule {}
