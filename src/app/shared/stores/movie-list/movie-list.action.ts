import { Movie, MovieListMode } from './movie-list.state';

export class AddMovie {
  static readonly type: string = '[MovieList] AddMovie';
  constructor(public movie: Movie) {}
}

export class RemoveMovie {
  static readonly type: string = '[MovieList] RemoveMovie';
  constructor(public id: number) {}
}

export class EditPriceMovie {
  static readonly type: string = '[MovieList] EditPriceMovie';
  constructor(public id: number, public price: number = 0) {}
}

export class AddMovieListNow {
  static readonly type: string = '[MovieList] AddMovieListNow';
  constructor(
    public movie_list: Movie[],
    public page: number = 1,
    public total_page: number = 1,
  ) {}
}

export class ChangeToAdminMode {
  static readonly type: string = '[MovieList] ChangeToAdminMode';
  constructor() {}
}

export class ChangeToUserMode {
  static readonly type: string = '[MovieList] ChangeToUserMode';
  constructor() {}
}

export class ChangeSearchText {
  static readonly type: string = '[MovieList] ChangeSearchText';
  constructor(public searchText: string) {}
}

export class ChangeMovieListMode {
  static readonly type: string = '[MovieList] ChangeMovieListMode';
  constructor(public movieListMode: MovieListMode) {}
}

export class NextPage {
  static readonly type: string = '[MovieList] NextPage';
  constructor() {}
}

export class PreviousPage {
  static readonly type: string = '[MovieList] PreviousPage';
  constructor() {}
}
