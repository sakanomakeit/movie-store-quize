export interface Movie {
  id: number;
  poster_path?: any;
  adult: boolean;
  overview: string;
  release_date: string;
  genre_ids: number[];
  original_title: string;
  original_language: string;
  title: string;
  backdrop_path?: any;
  popularity: number;
  vote_count: number;
  video: boolean;
  vote_average: number;
  price: number;
}

export interface MovieListStateModel {
  movie_list: Movie[];
  movie_list_now: Movie[];
  page: number;
  total_page: number;
  mode: MovieListMode;
  is_admin: boolean;
  search_text: string;
}

export enum MovieListMode {
  ALL,
  NO_PRICE_SPECIFIED,
}
