import {
  Action,
  Select,
  Selector,
  State,
  StateContext,
  Store,
} from '@ngxs/store';
import {
  AddMovie,
  AddMovieListNow,
  ChangeMovieListMode,
  ChangeSearchText,
  ChangeToAdminMode,
  ChangeToUserMode,
  EditPriceMovie,
  NextPage,
  PreviousPage,
  RemoveMovie,
} from './movie-list.action';
import { Injectable, OnDestroy } from '@angular/core';
import { Movie, MovieListMode, MovieListStateModel } from './movie-list.state';

import { MovieResponse } from './../../interfaces/movie-response.interface';
import { MovieResult } from '../../interfaces/movie-response.interface';
import { MovieService } from './../../services/movie.service';
import { Subscription } from 'rxjs';

@State<MovieListStateModel>({
  name: 'videoList',
  defaults: {
    movie_list: [],
    movie_list_now: [],
    page: 1,
    total_page: 1,
    mode: 0,
    is_admin: false,
    search_text: '',
  },
})
@Injectable()
export class MovieListState implements OnDestroy {
  $subscriptions: Subscription | null = null;

  constructor(
    private readonly movieService: MovieService,
    private readonly store: Store,
  ) {}

  @Selector() static $movieList(state: MovieListStateModel) {
    return state.movie_list;
  }

  @Selector() static $movieListNow(state: MovieListStateModel) {
    return state.movie_list_now;
  }

  @Selector() static $page(state: MovieListStateModel) {
    return state.page;
  }

  @Selector() static $totalPage(state: MovieListStateModel) {
    return state.total_page;
  }

  @Selector() static $mode(state: MovieListStateModel) {
    return state.mode;
  }

  @Selector() static $isAdmin(state: MovieListStateModel) {
    return state.is_admin;
  }

  @Selector() static $searchText(state: MovieListStateModel) {
    return state.search_text;
  }

  @Action(AddMovie)
  addMovie(ctx: StateContext<MovieListStateModel>, action: AddMovie) {
    const state: MovieListStateModel = ctx.getState();
    state.movie_list.push({ ...action.movie });
    ctx.setState({
      ...state,
      movie_list: state.movie_list,
    });
  }

  @Action(RemoveMovie)
  removeMovie(ctx: StateContext<MovieListStateModel>, action: RemoveMovie) {
    const state: MovieListStateModel = ctx.getState();
    state.movie_list = state.movie_list.filter(
      (movie: Movie) => movie.id !== action.id,
    );
    const index: number = state.movie_list_now.findIndex(
      (movie: Movie) => movie.id === action.id,
    );
    if (index > -1) state.movie_list_now[index].price = 0;

    ctx.setState({
      ...state,
      movie_list: state.movie_list,
      movie_list_now: state.movie_list_now,
    });
  }

  @Action(EditPriceMovie)
  editPriceMovie(
    ctx: StateContext<MovieListStateModel>,
    action: EditPriceMovie,
  ) {
    const state: MovieListStateModel = ctx.getState();
    const index: number = state.movie_list.findIndex(
      (movie: Movie) => movie.id === action.id,
    );
    const indexMovieNow: number = state.movie_list_now.findIndex(
      (movie: Movie) => movie.id === action.id,
    );

    if (action.price <= 0) {
      state.movie_list = state.movie_list.filter(
        (m: Movie, i: number) => i !== index,
      );
      state.movie_list_now[indexMovieNow].price = 0;
    } else {
      state.movie_list[index].price = action.price;
      state.movie_list_now[indexMovieNow].price = action.price;
    }

    ctx.setState({
      ...state,
      movie_list: state.movie_list,
      movie_list_now: state.movie_list_now,
    });
  }

  @Action(AddMovieListNow)
  addMovieListNow(
    ctx: StateContext<MovieListStateModel>,
    action: AddMovieListNow,
  ) {
    const state: MovieListStateModel = ctx.getState();
    state.movie_list_now = action.movie_list;
    state.page = action.page;
    state.total_page = action.total_page;

    ctx.setState({
      ...state,
      movie_list_now: state.movie_list_now,
      page: state.page,
      total_page: state.total_page,
    });
  }

  @Action(ChangeToAdminMode)
  changeToAdminMode(
    ctx: StateContext<MovieListStateModel>,
    action: ChangeToAdminMode,
  ) {
    const state: MovieListStateModel = ctx.getState();
    this.$subscriptions = this.movieService
      .getMoviesDiscover()
      .subscribe((result: MovieResponse) => {
        state.is_admin = true;
        this.onInitAdminMovie(
          result.results,
          state.movie_list,
          state.mode,
          result.page,
          result.total_pages,
        );

        ctx.setState({
          ...state,
          is_admin: state.is_admin,
          search_text: '',
        });
      });
  }

  @Action(ChangeToUserMode)
  changeToUserMode(
    ctx: StateContext<MovieListStateModel>,
    action: ChangeToUserMode,
  ) {
    const state: MovieListStateModel = ctx.getState();
    this.$subscriptions = this.movieService
      .getMoviesDiscover()
      .subscribe((result: MovieResponse) => {
        state.is_admin = false;
        this.onInitUserMovie(state.movie_list, result.page);

        ctx.setState({
          ...state,
          is_admin: state.is_admin,
          search_text: '',
        });
      });
  }

  @Action(ChangeSearchText)
  changeSearchText(
    ctx: StateContext<MovieListStateModel>,
    action: ChangeSearchText,
  ) {
    const state: MovieListStateModel = ctx.getState();
    if (state.is_admin) {
      if (action.searchText) {
        this.$subscriptions = this.movieService
          .getSearchMovies(action.searchText)
          .subscribe((result: MovieResponse) => {
            this.onInitAdminMovie(
              result.results,
              state.movie_list,
              state.mode,
              result.page,
              result.total_pages,
            );

            ctx.setState({
              ...state,
              search_text: action.searchText,
            });
          });
      } else {
        this.$subscriptions = this.movieService
          .getMoviesDiscover()
          .subscribe((result: MovieResponse) => {
            this.onInitAdminMovie(
              result.results,
              state.movie_list,
              state.mode,
              result.page,
              result.total_pages,
            );

            ctx.setState({
              ...state,
              search_text: action.searchText,
            });
          });
      }
    } else {
      if (action.searchText) {
        this.$subscriptions = this.movieService
          .getSearchMovies(action.searchText)
          .subscribe((result: MovieResponse) => {
            this.onInitUserSearchMovie(
              result.results,
              state.movie_list,
              result.page,
            );

            ctx.setState({
              ...state,
              search_text: action.searchText,
            });
          });
      } else {
        this.$subscriptions = this.movieService
          .getMoviesDiscover()
          .subscribe((result: MovieResponse) => {
            this.onInitUserMovie(state.movie_list, result.page);

            ctx.setState({
              ...state,
              search_text: action.searchText,
            });
          });
      }
    }
  }

  @Action(NextPage)
  nextPage(ctx: StateContext<MovieListStateModel>, action: NextPage) {
    const state: MovieListStateModel = ctx.getState();
    if (state.page + 1 > state.total_page) return;

    state.page++;
    ctx.setState({
      ...state,
      page: state.page,
    });

    if (state.is_admin) {
      if (state.search_text) {
        this.$subscriptions = this.movieService
          .getSearchMovies(state.search_text, state.page)
          .subscribe((result: MovieResponse) => {
            this.onInitAdminMovie(
              result.results,
              state.movie_list,
              state.mode,
              result.page,
              result.total_pages,
            );
          });
      } else {
        this.$subscriptions = this.movieService
          .getMoviesDiscover(state.page)
          .subscribe((result: MovieResponse) => {
            this.onInitAdminMovie(
              result.results,
              state.movie_list,
              state.mode,
              result.page,
              result.total_pages,
            );
          });
      }
    } else {
      if (state.search_text) {
        this.$subscriptions = this.movieService
          .getSearchMovies(state.search_text, state.page)
          .subscribe((result: MovieResponse) => {
            this.onInitUserSearchMovie(
              result.results,
              state.movie_list,
              result.page,
            );
          });
      } else {
        this.$subscriptions = this.movieService
          .getMoviesDiscover(state.page)
          .subscribe((result: MovieResponse) => {
            this.onInitUserMovie(state.movie_list, result.page);
          });
      }
    }
  }

  @Action(PreviousPage)
  previousPage(ctx: StateContext<MovieListStateModel>, action: PreviousPage) {
    const state: MovieListStateModel = ctx.getState();
    if (state.page - 1 <= 0) return;

    state.page--;
    ctx.setState({
      ...state,
      page: state.page,
    });

    if (state.is_admin) {
      if (state.search_text) {
        this.$subscriptions = this.movieService
          .getSearchMovies(state.search_text, state.page)
          .subscribe((result: MovieResponse) => {
            this.onInitAdminMovie(
              result.results,
              state.movie_list,
              state.mode,
              result.page,
              result.total_pages,
            );
          });
      } else {
        this.$subscriptions = this.movieService
          .getMoviesDiscover(state.page)
          .subscribe((result: MovieResponse) => {
            this.onInitAdminMovie(
              result.results,
              state.movie_list,
              state.mode,
              result.page,
              result.total_pages,
            );
          });
      }
    } else {
      if (state.search_text) {
        this.$subscriptions = this.movieService
          .getSearchMovies(state.search_text, state.page)
          .subscribe((result: MovieResponse) => {
            this.onInitUserSearchMovie(
              result.results,
              state.movie_list,
              result.page,
            );
          });
      } else {
        this.$subscriptions = this.movieService
          .getMoviesDiscover(state.page)
          .subscribe((result: MovieResponse) => {
            this.onInitUserMovie(state.movie_list, result.page);
          });
      }
    }
  }

  @Action(ChangeMovieListMode)
  changeMovieListMode(
    ctx: StateContext<MovieListStateModel>,
    action: ChangeMovieListMode,
  ) {
    const state: MovieListStateModel = ctx.getState();
    state.mode = action.movieListMode;
    state.page = 1;
    state.search_text = '';

    ctx.setState({
      ...state,
      mode: state.mode,
      page: state.page,
      search_text: state.search_text,
    });

    this.$subscriptions = this.movieService
      .getMoviesDiscover(state.page)
      .subscribe((result: MovieResponse) => {
        this.onInitAdminMovie(
          result.results,
          state.movie_list,
          state.mode,
          result.page,
          result.total_pages,
        );
      });
  }

  //TODO: Function
  onInitAdminMovie(
    newMovieListNow: MovieResult[] | Movie[],
    defaultMovieList: Movie[],
    mode: MovieListMode,
    page: number = 1,
    total_page: number = 1,
  ): void {
    const newListIds: number[] = newMovieListNow.map((m: any) => m.id);
    let defaultIdList: number[] = [];
    let findDefaultMovies: Movie[] | [] = [];

    if (mode == MovieListMode.NO_PRICE_SPECIFIED) {
      findDefaultMovies = defaultMovieList.filter(
        (movie: Movie) =>
          newListIds.includes(movie.id) &&
          movie.price !== null &&
          movie.price !== 0,
      );

      defaultIdList = findDefaultMovies.map((m: any) => m.id);
      newMovieListNow = newMovieListNow.filter(
        (movie: Movie) => !defaultIdList.includes(movie.id),
      );
    } else {
      findDefaultMovies = defaultMovieList.filter(
        (movie: Movie) =>
          newListIds.includes(movie.id) &&
          movie.price !== null &&
          movie.price !== 0,
      );

      for (let newMovie of newMovieListNow) {
        for (let defaultMovie of findDefaultMovies) {
          if (newMovie.id === defaultMovie.id) {
            newMovie.price = defaultMovie.price;
          }
        }
      }
    }

    this.store.dispatch(new AddMovieListNow(newMovieListNow, page, total_page));
  }

  onInitUserSearchMovie(
    newMovieListNow: MovieResult[] | Movie[],
    movieList: Movie[],
    page: number,
  ): void {
    let idsRes: number[] = newMovieListNow.map(({ id }: any) => id);
    let resultFilter = movieList.filter((m: any) => idsRes.includes(m.id));
    let totalPage: number =
      resultFilter.length / 8 < 1 ? 1 : Math.ceil(resultFilter.length / 8);
    let firstIndex: number = (page - 1) * 8;
    let lastIndex: number =
      firstIndex + 7 > totalPage ? firstIndex + 7 : totalPage;
    let movieListResult: any = resultFilter.slice(firstIndex, lastIndex);
    this.store.dispatch(new AddMovieListNow(movieListResult, page, totalPage));
  }

  onInitUserMovie(movieList: Movie[], page: number): void {
    let totalPage: number =
      movieList.length / 8 < 1 ? 1 : Math.ceil(movieList.length / 8);
    let firstIndex: number = (page - 1) * 8;
    let lastIndex: number =
      firstIndex + 7 > totalPage ? firstIndex + 7 : totalPage;
    let movieListResult: any = movieList.slice(firstIndex, lastIndex);
    this.store.dispatch(new AddMovieListNow(movieListResult, page, totalPage));
  }

  ngOnDestroy(): void {
    if (this.$subscriptions) {
      this.$subscriptions.unsubscribe();
    }
  }
}
