import { Movie } from './../movie-list/movie-list.state';

export interface ItemList {
  movie: Movie;
  price: number;
  number: number;
  total: number;
}

export interface CartStateModel {
  item_list: ItemList[];
  number_of_products: number;
  sub_total: number;
  discount: number;
  total: number;
}
