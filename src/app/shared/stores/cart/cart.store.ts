import { Action, State, StateContext } from '@ngxs/store';
import {
  AddItemToCart,
  ClearItem,
  IncreaseItemNumber,
  ReduceItemNumber,
  RemoveItemFromCart,
} from './cart.action';
import { CartStateModel, ItemList } from './cart.state';

import { Injectable } from '@angular/core';

@State<CartStateModel>({
  name: 'cart',
  defaults: {
    item_list: [],
    discount: 0,
    number_of_products: 0,
    sub_total: 0,
    total: 0,
  },
})
@Injectable()
export class CartState {
  constructor() {}

  @Action(AddItemToCart)
  addItemToCart(ctx: StateContext<CartStateModel>, action: AddItemToCart) {
    const state: CartStateModel = ctx.getState();
    const findId: number = state.item_list.findIndex(
      (item: ItemList) => item.movie.id === action.movie.id,
    );

    if (action.movie.price <= 0) return;

    if (findId > -1) {
      state.item_list[findId].number += 1;
      state.item_list[findId].total =
        state.item_list[findId].number * state.item_list[findId].price;
    } else {
      state.item_list.push({
        movie: action.movie,
        number: 1,
        price: action.movie.price,
        total: action.movie.price,
      });
    }
    state.number_of_products += 1;
    state.sub_total += action.movie.price;
    if (state.number_of_products >= 3 && state.number_of_products < 5) {
      state.discount = 10;
    }
    if (state.number_of_products >= 5) {
      state.discount = 20;
    }
    state.total = state.sub_total - (state.sub_total * state.discount) / 100;

    ctx.setState({
      ...state,
      item_list: state.item_list,
      number_of_products: state.number_of_products,
      sub_total: state.sub_total,
      discount: state.discount,
      total: state.total,
    });
  }

  @Action(RemoveItemFromCart)
  removeItemFromCart(
    ctx: StateContext<CartStateModel>,
    action: RemoveItemFromCart,
  ) {
    const state: CartStateModel = ctx.getState();
    const item: ItemList = state.item_list[action.index];

    state.number_of_products -= item.number;
    state.sub_total -= item.number * item.price;

    if (state.number_of_products >= 3 && state.number_of_products < 5) {
      state.discount = 10;
    } else if (state.number_of_products >= 5) {
      state.discount = 20;
    } else {
      state.discount = 0;
    }

    state.total = state.sub_total - (state.sub_total * state.discount) / 100;

    state.item_list = state.item_list.filter(
      (item, index) => index !== action.index,
    );

    ctx.setState({
      ...state,
      item_list: state.item_list,
      number_of_products: state.number_of_products,
      sub_total: state.sub_total,
      discount: state.discount,
      total: state.total,
    });
  }

  @Action(IncreaseItemNumber)
  increaseItemNumber(
    ctx: StateContext<CartStateModel>,
    action: IncreaseItemNumber,
  ) {
    const state: CartStateModel = ctx.getState();
    const item: ItemList = state.item_list[action.index];

    state.item_list[action.index].number += 1;
    state.item_list[action.index].total =
      state.item_list[action.index].price *
      state.item_list[action.index].number;

    state.number_of_products += 1;
    state.sub_total += item.price;

    if (state.number_of_products >= 3 && state.number_of_products < 5) {
      state.discount = 10;
    } else if (state.number_of_products >= 5) {
      state.discount = 20;
    } else {
      state.discount = 0;
    }

    state.total = state.sub_total - (state.sub_total * state.discount) / 100;

    ctx.setState({
      ...state,
      item_list: state.item_list,
      number_of_products: state.number_of_products,
      sub_total: state.sub_total,
      discount: state.discount,
      total: state.total,
    });
  }

  @Action(ReduceItemNumber)
  reduceItemNumber(
    ctx: StateContext<CartStateModel>,
    action: ReduceItemNumber,
  ) {
    const state: CartStateModel = ctx.getState();
    const item: ItemList = state.item_list[action.index];

    if (item.number > 1) {
      state.item_list[action.index].number -= 1;
      state.item_list[action.index].total =
        state.item_list[action.index].price *
        state.item_list[action.index].number;

      state.number_of_products -= 1;
      state.sub_total -= item.price;

      if (state.number_of_products >= 3 && state.number_of_products < 5) {
        state.discount = 10;
      } else if (state.number_of_products >= 5) {
        state.discount = 20;
      } else {
        state.discount = 0;
      }

      state.total = state.sub_total - (state.sub_total * state.discount) / 100;
    }

    ctx.setState({
      ...state,
      item_list: state.item_list,
      number_of_products: state.number_of_products,
      sub_total: state.sub_total,
      discount: state.discount,
      total: state.total,
    });
  }

  @Action(ClearItem)
  clearItem(ctx: StateContext<CartStateModel>, action: ClearItem) {
    const state: CartStateModel = ctx.getState();

    ctx.setState({
      ...state,
      item_list: [],
      discount: 0,
      number_of_products: 0,
      sub_total: 0,
      total: 0,
    });
  }
}
