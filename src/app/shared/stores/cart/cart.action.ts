import { Movie } from '../movie-list/movie-list.state';

export class AddItemToCart {
  static readonly type: string = '[Cart] AddItemToCart';
  constructor(public movie: Movie) {}
}

export class RemoveItemFromCart {
  static readonly type: string = '[Cart] RemoveItemFromCart';
  constructor(public index: number) {}
}

export class IncreaseItemNumber {
  static readonly type: string = '[Cart] IncreaseItemNumber';
  constructor(public index: number) {}
}

export class ReduceItemNumber {
  static readonly type: string = '[Cart] ReduceItemNumber';
  constructor(public index: number) {}
}

export class ClearItem {
  static readonly type: string = '[Cart] ClearItem';
  constructor() {}
}
