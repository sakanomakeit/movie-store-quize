export enum ActionPage {
  NEXT = 'NEXT',
  PRE = 'PRE',
  WAIT = 'WAIT',
}
