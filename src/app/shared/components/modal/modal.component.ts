import {
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit, OnChanges {
  @Input('show') isShow: boolean = false;
  @Input('header') header: string = 'Header';

  @Output() close: EventEmitter<void> = new EventEmitter<void>();
  isIn: boolean = true;
  timeOut: any = null;

  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(
    event: KeyboardEvent,
  ) {
    this.onToggle(false);
  }

  constructor() {}

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['isShow']) {
      window.clearTimeout(this.timeOut);
      if (this.isShow) {
        this.isShow = !this.isShow;
        this.onToggle(!this.isShow);
      } else {
        this.onToggle(this.isShow);
      }
    }
  }

  onToggle(state: boolean): void {
    this.isIn = state;
    this.isShow = state;
    this.timeOut = setTimeout(() => {
      if (!state) this.close.emit();
    }, 300);
  }
}
