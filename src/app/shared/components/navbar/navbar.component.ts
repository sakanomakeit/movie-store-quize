import {
  ChangeToAdminMode,
  ChangeToUserMode,
} from './../../stores/movie-list/movie-list.action';
import { Component, OnInit } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { filter, map } from 'rxjs/operators';

import { CartState } from './../../stores/cart/cart.store';
import { CartStateModel } from './../../stores/cart/cart.state';
import { Observable } from 'rxjs';

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  @Select(CartState)
  $cart: Observable<CartStateModel>;

  isAdmin: boolean = false;

  constructor(private router: Router, private store: Store) {}

  ngOnInit() {
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationStart),
        map((event) => event as NavigationStart),
      )
      .subscribe((event) => {
        if (event.url.split('/')[1] === 'admin') this.isAdmin = true;
        else this.isAdmin = false;
      });
  }

  onChangeToUser(): void {
    this.store.dispatch(new ChangeToUserMode());
  }

  onChangeToAdmin(): void {
    this.store.dispatch(new ChangeToAdminMode());
  }
}
