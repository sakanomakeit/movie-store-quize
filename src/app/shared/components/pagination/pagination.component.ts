import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import {
  NextPage,
  PreviousPage,
} from './../../stores/movie-list/movie-list.action';
import { Observable, Subscription } from 'rxjs';
import { Select, Store } from '@ngxs/store';

import { MovieListState } from 'src/app/shared/stores/movie-list/movie-list.store';
import { MovieListStateModel } from '../../stores/movie-list/movie-list.state';

@Component({
  selector: 'pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
})
export class PaginationComponent implements OnInit, OnDestroy {
  @Output() next: EventEmitter<void> = new EventEmitter<void>();
  @Output() pre: EventEmitter<void> = new EventEmitter<void>();

  @Select(MovieListState)
  $movieList: Observable<MovieListStateModel>;
  $subscriptions: Subscription | null = null;

  totalPage: number = 1;
  page: number = 1;

  constructor(private store: Store) {}

  ngOnInit() {
    this.$movieList.subscribe((res) => {
      this.totalPage = res.total_page;
      this.page = res.page;
    });
  }

  onNext(): void {
    this.next.emit();
    this.store.dispatch(new NextPage());
  }

  onPre(): void {
    this.pre.emit();
    this.store.dispatch(new PreviousPage());
  }

  ngOnDestroy(): void {
    this.$subscriptions?.unsubscribe();
  }
}
