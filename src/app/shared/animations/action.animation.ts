import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';

export const ActionAnimation: any = [
  trigger('ActionPageAnimate', [
    state('NEXT', style({ opacity: 1, transform: 'translateX(0)' })),
    transition('* => NEXT', [
      animate(
        '250ms ease-out',
        style({ opacity: 1, transform: 'translateX(0)' }),
      ),
      animate(
        '250ms ease-out',
        style({ opacity: 0, transform: 'translateX(-200px)' }),
      ),
      animate(
        '100ms ease-in',
        style({ opacity: 0, transform: 'translateX(200px)' }),
      ),
      animate(
        '250ms ease-in',
        style({ opacity: 1, transform: 'translateX(0)' }),
      ),
    ]),
    state('PRE', style({ opacity: 1, transform: 'translateX(0)' })),
    transition('* => PRE', [
      animate(
        '250ms ease-out',
        style({ opacity: 1, transform: 'translateX(0)' }),
      ),
      animate(
        '250ms ease-out',
        style({ opacity: 0, transform: 'translateX(200px)' }),
      ),
      animate(
        '100ms ease-in',
        style({ opacity: 0, transform: 'translateX(-200px)' }),
      ),
      animate(
        '250ms ease-in',
        style({ opacity: 1, transform: 'translateX(0)' }),
      ),
    ]),
  ]),
];
