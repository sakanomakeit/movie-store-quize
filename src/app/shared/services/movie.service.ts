import {
  ErrorResponse,
  MovieResponse,
} from './../interfaces/movie-response.interface';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const API_KEY = 'api_key=d7e291b341f0f22e7d98ad78b8042698';
const BASE_URL = 'https://api.themoviedb.org/3';
const API_URL = BASE_URL + '/discover/movie?sort_by=popularity.desc&' + API_KEY;
const SEARCH_URL = BASE_URL + '/search/movie?' + API_KEY;
const BY_ID_URL = BASE_URL + '/movie/';

export const IMG_URL = 'https://image.tmdb.org/t/p/w500';

@Injectable({
  providedIn: 'root',
})
export class MovieService {
  constructor(private http: HttpClient) {}

  getMoviesDiscoverById(id: number): Observable<any> {
    return this.http.get<MovieResponse | ErrorResponse>(
      BY_ID_URL + id + '?' + API_KEY,
    );
  }

  getMoviesDiscover(page: number = 1): Observable<any> {
    return this.http.get<MovieResponse | ErrorResponse>(
      API_URL + '&page=' + page,
    );
  }

  getSearchMovies(search: string, page: number = 1): Observable<any> {
    return this.http.get<MovieResponse | ErrorResponse>(
      SEARCH_URL + '&query=' + search + '&page=' + page,
    );
  }
}
