import { RouterModule, Routes } from '@angular/router';

import { NgModule } from '@angular/core';

const routes: Routes = [
  { path: '', redirectTo: 'user', pathMatch: 'full' },
  {
    path: 'admin',
    loadChildren: () =>
      import('./pages/admins/admins.module').then((m) => m.AdminsModule),
  },
  {
    path: 'user',
    loadChildren: () =>
      import('./pages/users/users.module').then((m) => m.UsersModule),
  },
  { path: '**', redirectTo: 'user', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
