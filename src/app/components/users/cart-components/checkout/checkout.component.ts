import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { Select, Store } from '@ngxs/store';

import { CartState } from './../../../../shared/stores/cart/cart.store';
import { CartStateModel } from 'src/app/shared/stores/cart/cart.state';
import { ClearItem } from './../../../../shared/stores/cart/cart.action';
import { Observable } from 'rxjs';

@Component({
  selector: 'checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss'],
})
export class CheckoutComponent implements OnInit, OnChanges {
  @Select(CartState)
  $cart: Observable<CartStateModel>;

  @Input() isStartTimeOut: boolean = false;
  @Output() isSubmit: EventEmitter<void> = new EventEmitter<void>();
  @Output() isCancel: EventEmitter<void> = new EventEmitter<void>();
  @Output() isTimeOut: EventEmitter<void> = new EventEmitter<void>();

  isThank: boolean = false;

  timeOut: number = 1000;
  defaultTimeOut: number = 60;
  time: number = this.defaultTimeOut;
  intervalTimeOut: any = null;
  windowTimeOut: any = null;

  constructor(private store: Store) {}

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['isStartTimeOut']) {
      if (this.isStartTimeOut) this.onStartTimeOut();
      else this.onClearAllWindow();
    }
  }

  onStartTimeOut(): void {
    this.onClearAllWindow();
    this.time = this.defaultTimeOut;

    this.intervalTimeOut = setInterval(() => {
      this.time--;
    }, this.timeOut);

    this.windowTimeOut = setTimeout(() => {
      this.onClearAllWindow();
      this.isStartTimeOut = false;
      this.isTimeOut.emit();
    }, this.defaultTimeOut * this.timeOut + 1000);
  }

  onSubmit(): void {
    this.isThank = true;
    this.windowTimeOut = setTimeout(() => {
      this.store.dispatch(new ClearItem());
      this.onClearAllWindow();
      this.isThank = false;
      this.isSubmit.emit();
    }, 1000);
  }

  onCancel(): void {
    this.onClearAllWindow();
    this.isCancel.emit();
  }

  onClearAllWindow(): void {
    window.clearInterval(this.intervalTimeOut);
    window.clearTimeout(this.windowTimeOut);
    this.isThank = false;
  }

  getTimeOut(): string {
    let minutes: string = parseInt((this.time / 60).toString(), 10).toString();
    let seconds: string = parseInt((this.time % 60).toString(), 10).toString();

    minutes = parseInt(minutes) < 10 ? '0' + minutes : minutes;
    seconds = parseInt(seconds) < 10 ? '0' + seconds : seconds;

    return `${minutes}:${seconds}`;
  }
}
