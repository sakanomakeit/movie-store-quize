import { CheckoutComponent } from './checkout/checkout.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [CommonModule],
  declarations: [CheckoutComponent],
  exports: [CheckoutComponent],
})
export class CartComponentsModule {}
