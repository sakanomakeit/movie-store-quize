import { CommonModule } from '@angular/common';
import { MovieListComponent } from './movie-list/movie-list.component';
import { NgModule } from '@angular/core';
import { PaginationComponent } from '../../../shared/components/pagination/pagination.component';
import { SearchInputComponent } from './search-input/search-input.component';

@NgModule({
  imports: [CommonModule],
  declarations: [SearchInputComponent, MovieListComponent],
  exports: [SearchInputComponent, MovieListComponent],
})
export class MoviesDashboardComponentsModule {}
