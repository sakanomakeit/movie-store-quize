import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  Movie,
  MovieListMode,
  MovieListStateModel,
} from 'src/app/shared/stores/movie-list/movie-list.state';
import { Observable, Subject, Subscription } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { debounceTime, switchMap } from 'rxjs/operators';

import { AddMovieListNow } from 'src/app/shared/stores/movie-list/movie-list.action';
import { ChangeSearchText } from './../../../../shared/stores/movie-list/movie-list.action';
import { MovieListState } from 'src/app/shared/stores/movie-list/movie-list.store';
import { MovieResult } from 'src/app/shared/interfaces/movie-response.interface';
import { MovieService } from 'src/app/shared/services/movie.service';

@Component({
  selector: 'search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss'],
})
export class SearchInputComponent implements OnInit, OnDestroy {
  @Select(MovieListState)
  $movieList: Observable<MovieListStateModel>;
  $subscriptions: Subscription | null = null;

  @Select(MovieListState.$searchText)
  $searchText: Observable<MovieListMode>;

  searchText: Subject<string> = new Subject<string>();

  constructor(private store: Store) {}

  ngOnInit() {
    this.searchText.pipe(debounceTime(400)).subscribe((result) => {
      this.store.dispatch(new ChangeSearchText(result));
    });
  }

  // onInitMovie(res: MovieResult[] | Movie[]): void {
  //   let result: any = null;
  //   this.$subscriptions = this.$movieList.subscribe((value) => {
  //     result = value;
  //   });
  //   let idsRes: number[] = res.map(({ id }: any) => id);
  //   let resultFilter = result.movie_list.filter((m: any) =>
  //     idsRes.includes(m.id),
  //   );

  //   let page: number = result.page;
  //   let totalPage: number =
  //     resultFilter.length / 10 < 1 ? 1 : Math.ceil(resultFilter.length / 10);
  //   let firstIndex: number = (page - 1) * 10;
  //   let lastIndex: number =
  //     firstIndex + 9 > totalPage ? firstIndex + 9 : totalPage;
  //   let movieList: any = resultFilter.slice(firstIndex, lastIndex);
  //   this.store.dispatch(new AddMovieListNow(movieList, page, totalPage));
  // }

  // onInitMovieDefault(): void {
  //   let result: any = null;
  //   this.$subscriptions = this.$movieList.subscribe((value) => {
  //     result = value;
  //   });
  //   let page: number = result.page;
  //   let totalPage: number =
  //     result.movie_list.length / 10 < 1
  //       ? 1
  //       : Math.ceil(result.movie_list.length / 10);
  //   let firstIndex: number = (page - 1) * 10;
  //   let lastIndex: number =
  //     firstIndex + 9 > totalPage ? firstIndex + 9 : totalPage;
  //   let movieList: any = result.movie_list.slice(firstIndex, lastIndex);
  //   this.store.dispatch(new AddMovieListNow(movieList, page, totalPage));
  // }

  onSearch(search: string): void {
    this.searchText.next(search);
  }

  ngOnDestroy(): void {
    this.$subscriptions?.unsubscribe();
  }
}
