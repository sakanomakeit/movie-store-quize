import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  Movie,
  MovieListStateModel,
} from 'src/app/shared/stores/movie-list/movie-list.state';
import { Observable, Subscription } from 'rxjs';
import { Select, Store } from '@ngxs/store';

import { AddItemToCart } from './../../../../shared/stores/cart/cart.action';
import { IMG_URL } from './../../../../shared/services/movie.service';
import { MovieListState } from 'src/app/shared/stores/movie-list/movie-list.store';
import { MovieService } from 'src/app/shared/services/movie.service';

@Component({
  selector: 'movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss'],
})
export class MovieListComponent implements OnInit, OnDestroy {
  @Select(MovieListState.$movieListNow)
  $movieListNow: Observable<Movie[]>;
  @Select(MovieListState.$page)
  $page: Observable<number>;
  @Select(MovieListState.$totalPage)
  $totalPage: Observable<number>;
  $subscriptions: Subscription | null = null;

  imageBaseUrl: string = IMG_URL;

  constructor(private store: Store) {}

  ngOnInit() {}

  onAddToCart(movie: Movie): void {
    this.store.dispatch(new AddItemToCart(movie));
  }

  ngOnDestroy(): void {
    if (this.$subscriptions) this.$subscriptions.unsubscribe();
  }
}
