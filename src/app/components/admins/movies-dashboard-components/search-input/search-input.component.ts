import {
  AddMovieListNow,
  ChangeMovieListMode,
  ChangeSearchText,
} from './../../../../shared/stores/movie-list/movie-list.action';
import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  Movie,
  MovieListMode,
} from './../../../../shared/stores/movie-list/movie-list.state';
import { Observable, Subject, Subscription } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
  switchMap,
} from 'rxjs/operators';

import { MovieListState } from './../../../../shared/stores/movie-list/movie-list.store';
import { MovieListStateModel } from 'src/app/shared/stores/movie-list/movie-list.state';
import { MovieResult } from 'src/app/shared/interfaces/movie-response.interface';
import { MovieService } from './../../../../shared/services/movie.service';

@Component({
  selector: 'search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss'],
})
export class SearchInputComponent implements OnInit, OnDestroy {
  isAll: boolean = true;
  isNotPrice: boolean = false;
  searchText: Subject<string> = new Subject<string>();
  searchTextDefault: string = '';
  page: number = 1;

  @Select(MovieListState.$mode)
  $mode: Observable<MovieListMode>;

  @Select(MovieListState.$searchText)
  $searchText: Observable<MovieListMode>;

  $subscriptions: Subscription | null = null;

  constructor(private movieService: MovieService, private store: Store) {}

  ngOnInit() {
    this.$mode.subscribe((result: MovieListMode) => {
      const mode: MovieListMode = result;
      this.onReset();
      if (mode === MovieListMode.NO_PRICE_SPECIFIED) {
        this.isNotPrice = true;
      } else {
        this.isAll = true;
      }
    });

    this.searchText.pipe(debounceTime(400)).subscribe((result) => {
      this.store.dispatch(new ChangeSearchText(result));
    });
  }

  // onInitMovie(
  //   res: MovieResult[] | Movie[],
  //   page: number = 1,
  //   total_page: number = 1,
  // ): void {
  //   let defaultValue: any = null;
  //   let ids: number[] = res.map((m: any) => m.id);
  //   this.$subscriptions = this.$movieList.subscribe((value) => {
  //     defaultValue = value;
  //   });

  //   let finds: Movie[] | [] = [];

  //   if (defaultValue.mode == MovieListMode.NO_PRICE_SPECIFIED) {
  //     finds = defaultValue.movie_list.filter(
  //       (movie: Movie) =>
  //         ids.includes(movie.id) && movie.price !== null && movie.price !== 0,
  //     );
  //     ids = finds.map((m: any) => m.id);
  //     res = res.filter((movie: Movie) => !ids.includes(movie.id));
  //   } else {
  //     finds = defaultValue.movie_list.filter(
  //       (movie: Movie) =>
  //         ids.includes(movie.id) && movie.price !== null && movie.price !== 0,
  //     );
  //     for (let movieNow of res) {
  //       for (let movie of finds) {
  //         if (movieNow.id === movie.id) {
  //           movieNow.price = movie.price;
  //         }
  //       }
  //     }
  //   }
  //   this.store.dispatch(new AddMovieListNow(res, page, total_page));
  // }

  onReset(): void {
    this.isAll = false;
    this.isNotPrice = false;
  }

  onAll(): void {
    this.onReset();
    this.isAll = true;
    this.store.dispatch(new ChangeMovieListMode(MovieListMode.ALL));
  }

  onNotPrice(): void {
    this.onReset();
    this.isNotPrice = true;
    this.store.dispatch(
      new ChangeMovieListMode(MovieListMode.NO_PRICE_SPECIFIED),
    );
  }

  onSearch(search: string): void {
    this.searchText.next(search);
    this.searchTextDefault = search;
  }

  ngOnDestroy(): void {
    if (this.$subscriptions) {
      this.$subscriptions.unsubscribe();
    }
  }
}
