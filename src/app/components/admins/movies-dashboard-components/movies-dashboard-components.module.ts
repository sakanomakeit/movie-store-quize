import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MovieListComponent } from './movie-list/movie-list.component';
import { NgModule } from '@angular/core';
import { SearchInputComponent } from './search-input/search-input.component';

@NgModule({
  imports: [CommonModule, FormsModule],
  declarations: [SearchInputComponent, MovieListComponent],
  exports: [SearchInputComponent, MovieListComponent],
})
export class MoviesDashboardComponentsModule {}
