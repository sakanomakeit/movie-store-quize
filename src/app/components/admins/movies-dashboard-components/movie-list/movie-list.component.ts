import {
  AddMovie,
  EditPriceMovie,
  RemoveMovie,
} from './../../../../shared/stores/movie-list/movie-list.action';
import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  IMG_URL,
  MovieService,
} from './../../../../shared/services/movie.service';
import {
  MovieListMode,
  MovieListStateModel,
} from './../../../../shared/stores/movie-list/movie-list.state';
import { Observable, Subscription } from 'rxjs';
import { Select, Store } from '@ngxs/store';

import { Movie } from './../../../../shared/stores/movie-list/movie-list.state';
import { MovieListState } from './../../../../shared/stores/movie-list/movie-list.store';
import { Router } from '@angular/router';

@Component({
  selector: 'movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss'],
})
export class MovieListComponent implements OnInit, OnDestroy {
  @Select(MovieListState.$movieList)
  $movieList: Observable<Movie[]>;
  @Select(MovieListState.$movieListNow)
  $movieListNow: Observable<Movie[]>;
  @Select(MovieListState.$page)
  $page: Observable<number>;
  @Select(MovieListState.$totalPage)
  $totalPage: Observable<number>;

  $subscriptions: Subscription | null = null;
  editIndex: number | null = null;

  priceMovie: number = 0;
  mode: MovieListMode = 0;
  page: number = 1;
  defaultMovieListNow: Movie[] = [];

  imageBaseUrl: string = IMG_URL;
  constructor(private store: Store) {}

  ngOnInit() {}

  onEdit(index: number, price: number = 0): void {
    this.editIndex = index;
    this.priceMovie = price;
  }

  onRemove(id: number): void {
    this.store.dispatch(new RemoveMovie(id));
  }

  async onSave(id: any, value: number, movie: Movie): Promise<void> {
    let result: Movie[];
    this.$subscriptions = this.$movieList.subscribe((value) => {
      result = value;
    });
    const find: Movie | undefined = result!.find(
      (movie: Movie) => movie.id === id,
    );

    if (find === undefined) {
      this.store.dispatch(new AddMovie(movie));
      this.store.dispatch(new EditPriceMovie(id, value));
    } else {
      this.store.dispatch(new EditPriceMovie(id, value));
    }
    this.editIndex = null;
    this.priceMovie = 0;
  }

  onCancel(): void {
    this.editIndex = null;
    this.priceMovie = 0;
  }

  ngOnDestroy(): void {
    if (this.$subscriptions) {
      this.$subscriptions.unsubscribe();
    }
  }
}
